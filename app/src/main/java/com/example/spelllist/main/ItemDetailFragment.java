package com.example.spelllist.main;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.spelllist.R;
import com.example.spelllist.spell.SpellList;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private SpellList.Spell mItem;

    private TextView textView;

    private UpdaterInterface updater;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {

    }

    /*
    This is where I load from my list of spells, I believe
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = SpellList.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.getName());
            }
        }

        setHasOptionsMenu(true);
    }

    //Setter function for interface
    public void setUpdater(UpdaterInterface that){
        updater = that;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            (textView = rootView.findViewById(R.id.item_detail)).setText(Html.fromHtml(mItem.toString()));
        }

        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_list_all:
                onDeleteListAll();
                return true;
            case R.id.deleteItem:
                onDeleteItem();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onDeleteItem() {
        SpellList.deleteSingularSpell(mItem);
        textView.setText(null);
        updater.updateList();
    }


    private void onDeleteListAll(){
        SpellList.deleteThemAll();
        //getActivity().getSupportFragmentManager().popBackStack();
        textView.setText(null);
        updater.updateList();
    }


    //The interface
    public interface UpdaterInterface{
        //The overridden function for the interface
        void updateList();
    }
}
