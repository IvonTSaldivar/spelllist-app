package com.example.spelllist.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.spelllist.R;
import com.example.spelllist.utility.CustomSpinnerAdapter;
import com.example.spelllist.utility.RecyclerItemTouchHelper;
import com.example.spelllist.spell.SpellList;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @TODO Add support for multiple Characters/Spell lists
 * @TODO Branch the project so there will always be a working version of the app
 *
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ItemListActivity extends AppCompatActivity implements ItemDetailFragment.UpdaterInterface, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private SimpleItemRecyclerViewAdapter mAdapter;

    private static WeakReference<Context> mContext;

    View recyclerView;

    public static Context returnContext(){
        return mContext.get();
    }
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = new WeakReference<>(this.getApplicationContext());

        setContentView(R.layout.activity_item_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle() + " Toolbar");


        FloatingActionButton addEntryButton = findViewById(R.id.addEntryButton);
        addEntryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, SpellEntryActivity.class);
                context.startActivity(intent);
            }
        });
        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        //SpellList.readSpellsFromFile(this);

        recyclerView = findViewById(R.id.item_list);
        assert recyclerView != null;
        RecyclerView realRecyclerView = setupRecyclerView((RecyclerView) recyclerView);


        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(realRecyclerView);


    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }


    private RecyclerView setupRecyclerView(@NonNull RecyclerView recyclerView) {
        mAdapter = new SimpleItemRecyclerViewAdapter(this, SpellList.ITEMS, mTwoPane);
        recyclerView.setAdapter(mAdapter);
        return recyclerView;
    }

    @Override
    public void updateList() {
        Collections.sort(SpellList.ITEMS);
        setupRecyclerView((RecyclerView)recyclerView);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof SimpleItemRecyclerViewAdapter.ViewHolder){
            if (direction == ItemTouchHelper.LEFT){
                //Increment
                mAdapter.incrementSpell(viewHolder.getAdapterPosition());
            } else if (direction == ItemTouchHelper.RIGHT){
                //Decrement
                mAdapter.decrementSpell(viewHolder.getAdapterPosition());
            }
        }
    }


    /**
     *
     * Class SimpleItemRecycler
     *
     *
     */

    public static class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final ItemListActivity mParentActivity;
        private final List<SpellList.Spell> mValues;
        private final boolean mTwoPane;

        private final View.OnLongClickListener mOnLongClickListener = new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                SpellList.Spell item = (SpellList.Spell) v.getTag();
                if(item.getPrepared() != 0){
                    item.setPrepared(item.getPrepared() * -1);
                    System.out.println("Long Click");
                    notifyItemChanged(mValues.indexOf(item));
                    return true;
                }
                return false;
            }
        };

        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpellList.Spell item = (SpellList.Spell) view.getTag();
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putString(ItemDetailFragment.ARG_ITEM_ID, item.getID());
                    ItemDetailFragment fragment = new ItemDetailFragment();
                    fragment.setUpdater(mParentActivity);
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.item_detail_container, fragment)
                            .commit();
                } else {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, ItemDetailActivity.class);
                    intent.putExtra(ItemDetailFragment.ARG_ITEM_ID, item.getID());

                    context.startActivity(intent);
                }
            }
        };


        SimpleItemRecyclerViewAdapter(ItemListActivity parent,
                                      List<SpellList.Spell> items,
                                      boolean twoPane) {
            mValues = items;
            mParentActivity = parent;
            mTwoPane = twoPane;


        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mIdView.setText(mValues.get(position).getLevel());
            holder.mContentView.setText(mValues.get(position).getName());
            holder.mPrepared.setText("(" + mValues.get(position).getPrepared() + ")");


            holder.itemView.setTag(mValues.get(position));
            holder.itemView.setOnClickListener(mOnClickListener);
            holder.itemView.setOnLongClickListener(mOnLongClickListener);
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }


        public void incrementSpell(int pos){
            mValues.get(pos).setPrepared(1);
            notifyItemChanged(pos);
        }

        public void decrementSpell(int pos){
            if(mValues.get(pos).getPrepared() != 0)
                mValues.get(pos).setPrepared(-1);
            notifyItemChanged(pos);
        }


        /**
         *
         * Class ViewHolder
         *
         *
         */

        public class ViewHolder extends RecyclerView.ViewHolder {
            final TextView mIdView;
            final TextView mContentView;
            final TextView mPrepared;

            public RelativeLayout viewBackground;
            public RelativeLayout viewForeground;



            ViewHolder(View view) {
                super(view);
                mIdView = view.findViewById(R.id.id_text);
                mContentView = view.findViewById(R.id.content);
                mPrepared = view.findViewById(R.id.prepared);
                viewBackground = view.findViewById(R.id.view_background);
                viewForeground = view.findViewById(R.id.view_foreground);
            }
        }
    }
}
