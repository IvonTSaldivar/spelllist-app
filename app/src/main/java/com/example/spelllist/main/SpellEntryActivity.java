package com.example.spelllist.main;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.spelllist.R;
import com.example.spelllist.main.ItemListActivity;
import com.example.spelllist.spell.SpellList;
import com.example.spelllist.utility.CustomSpinnerAdapter;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SpellEntryActivity extends AppCompatActivity {

    private static String spinnerValue = "invalid";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_spell_entry);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final EditText urlTextEntry = findViewById(R.id.urlTextEntry);
        final Button submitButton = findViewById(R.id.submitURLButton);

        addItemsToSpinner();


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!urlTextEntry.getText().toString().isEmpty())
                    new SpellFetcher(view).execute(urlTextEntry.getText().toString());
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
//        Spinner spinner = findViewById(R.id.class_spinner);
//        spinner.setEnabled(false);
    }

    public void addItemsToSpinner() {
        Spinner spinner = findViewById(R.id.class_spinner);

        ArrayList<String> list = new ArrayList<String>();
        list.add("wizard");
        list.add("witch");
        list.add("warpriest");
        list.add("summoner");
        list.add("summoner (unchained)");
        list.add("spiritualist");
        list.add("sorcerer");
        list.add("skald");
        list.add("shaman");
        list.add("sahirafiyun");
        list.add("redmantisassassin");
        list.add("ranger");
        list.add("psychic");
        list.add("paladin");
        list.add("oracle");
        list.add("occultist");
        list.add("mesmerist");
        list.add("medium");
        list.add("magus");
        list.add("investigator");
        list.add("inquisitor");
        list.add("hunter");
        list.add("druid");
        list.add("cleric");
        list.add("bloodrager");
        list.add("bard");
        list.add("assassin");
        list.add("arcanist");
        list.add("antipaladin");
        list.add("alchemist");
        //Collections.sort(list, Collections.<String>reverseOrder());
        // Custom ArrayAdapter with spinner item layout to set popup background

        final CustomSpinnerAdapter spinAdapter = new CustomSpinnerAdapter(
                getApplicationContext(), list);

        spinner.setAdapter(spinAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                if(adapter.getItemAtPosition(position).toString().equals("summoner (unchained)")){
                    spinnerValue = "summoner \\(unchained\\)";
                } else {
                    spinnerValue = adapter.getItemAtPosition(position).toString();
                }

                // Showing selected spinner item
//            Toast.makeText(getApplicationContext(), "Selected: " + spinnerValue,
//                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

    }

    private static class SpellFetcher extends AsyncTask<String, Void, JSONObject> {
        private WeakReference<View> rootView;


        SpellFetcher(View view) {
            rootView = new WeakReference<>(view);
        }

       /*
        @Override
        protected void onPreExecute(){
            this.getClass();
        }
        */

        @Override
        protected JSONObject doInBackground(String... strings) {
            return aonURL(strings[0]);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            if (result.has("Borked")) {
                System.out.println("Borked was returned");
                Snackbar.make(rootView.get(), "Something went wrong :'(", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Action", null).show();
                return;
            }

            try {
                result.put("level_value", spinnerValue);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            SpellList.Spell newSpell = new SpellList.Spell(result);
            SpellList.writeSpellsToFile(rootView.get().getContext());

            System.out.println("--------Printing Spell.toString()---------");
            System.out.println(newSpell.toString());

            Snackbar.make(rootView.get(), "Done!", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Action", null).show();


        }


        private JSONObject aonURL(String sourceURL) {
            try {
                if (!sourceURL.contains("aonprd.com"))
                    sourceURL = "http://aonprd.com/SpellDisplay.aspx?ItemName=" + sourceURL;
                JSONObject returnVal = new JSONObject();
                Document aon = Jsoup.connect(sourceURL).get();


                String regexNameSchoolLevels = "(?:<h1 class=\"title\">[^>]+>) ([\\w\\s\\d\\[\\]\\(\\)" +
                        "/.,;+'-]+)(?:.*?<b>School<\\/b>([\\w\\s\\d\\[\\]\\(\\)/.,;+-]+))(?:.*?<b>Level" +
                        "<\\/b>([\\w\\s\\d\\[\\]\\(\\)/.,;+-]+))";

                String regexCastingComponents = "(?:<.*?<b>Casting Time<\\/b>)([\\w\\s\\d\\[\\]\\(\\)/.,;+-]+)" +
                        ".*?<b>Components<\\/b>([\\w\\s\\d\\[\\]\\(\\)/.,;+-]+)?";

                String regexRangeTargets = ".*?<b>Range<\\/b>([\\w\\s\\d\\[\\]\\(\\)/.,;+-]+)" +
                        "(?:<br><b>([\\w\\s\\d\\[\\]\\(\\)/.,;+-]+)<\\/b>([\\w\\s\\d\\[\\]\\(\\)/.,;+-]+)<br>)" +
                        "(?:.*?<b>Duration<\\/b>([\\w\\s\\d\\[\\]\\(\\)/.,;+-]+))";

                String regexSavesSR = ".*?<b>Saving Throw<\\/b> ?([\\w\\s\\d\\[\\]\\(\\)/.,;+-]+)" +
                        ".*?<b>Spell Resistance<\\/b> ?([\\w\\s\\d\\[\\]\\(\\)/.,;+-]+) ?<";



                Element theBigSpan = aon.getElementById("ctl00_MainContent_DataListTypes");
                long start = System.currentTimeMillis();
                System.out.println("Started timer");

                //One

                Pattern regexPattern = Pattern.compile(regexNameSchoolLevels);
                Matcher m = regexPattern.matcher(theBigSpan.outerHtml());
                try {
                    if (m.find()) {
                        returnVal.put("name", m.group(1));
                        returnVal.put("school", m.group(2));
                        returnVal.put("level", m.group(3));
                    } else System.out.println("!m.find() regexNameSchoolLevels");
                } catch (Exception e) {
                    try {
                        returnVal.put("Borked", true);
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    System.out.println("Broke doing regexNameSchoolLevels");
                }

                try {
                    regexPattern = Pattern.compile(regexCastingComponents);
                    m = regexPattern.matcher(theBigSpan.outerHtml());
                    if (m.find()) {
                        returnVal.put("casting_time", m.group(1));
                        returnVal.put("components", m.group(2));
                    } else System.out.println("!m.find() regexCastingComponents");
                } catch (Exception e) {
                    System.out.println("Broke doing regexCastingComponents");
                }

                try {
                    regexPattern = Pattern.compile(regexRangeTargets);
                    m = regexPattern.matcher(theBigSpan.outerHtml());
                    if (m.find()) {
                        returnVal.put("range", m.group(1));
                        returnVal.put("target_tag", m.group(2));
                        returnVal.put("target", m.group(3));
                    } else System.out.println("!m.find() regexRangeTargets");
                } catch (Exception e) {
                    System.out.println("Broke doing regexRangeTargets");
                }

                try {
                    regexPattern = Pattern.compile(regexSavesSR);
                    m = regexPattern.matcher(theBigSpan.outerHtml());
                    if (m.find()) {
                        returnVal.put("saving_throw", m.group(1));
                        returnVal.put("spell_resistance", m.group(2));
                    } else System.out.println("!m.find() regexSavesSR");
                } catch (Exception e) {
                    System.out.println("Broke doing regexSavesSR");
                }


                String regexDescriptionFull = ".*?<h3 class=\"framing\">Description<\\/h3>(.*?)(?:<\\/span>|<h[1-2])";
                String regexDescriptionP1 = "(?:.*?<h3 class=\"framing\">Description<\\/h3>(.*))";
                String regexDescriptionP2 = "(<ul>(?:\\s*?.*?<li>.*?<\\/li>)+?\\s+<\\/ul>.*?)(?:<\\/span>|<h[1-2])";

                try {
                    if (!theBigSpan.outerHtml().contains("<ul>")) {
                        System.out.println("Did not find <ul>");
                        regexPattern = Pattern.compile(regexDescriptionFull);
                        m = regexPattern.matcher(theBigSpan.outerHtml());
                        if (m.find()) {
                            returnVal.put("description", m.group(1));
                            //.replaceAll("<br>", "\n"));
                            //.replaceAll("(\\s?<i>\\s?)", " *")
                            //.replaceAll("(\\s?<\\/i>\\s?)", "* "));
                            System.out.println("It got the description: " + m.group(1));
                        } else {
                            System.out.println("!m.find() regexDescription no <ul>");
                            System.out.println(theBigSpan.outerHtml());
                        }
                    } else {
                        regexPattern = Pattern.compile(regexDescriptionP1);
                        m = regexPattern.matcher(theBigSpan.outerHtml());
                        StringBuilder description = new StringBuilder();
                        if(m.find()){
                            description.append(m.group(1));
                        }
                        regexPattern = Pattern.compile(regexDescriptionP2);
                        m = regexPattern.matcher(theBigSpan.outerHtml());
                        if(m.find()){
                            description.append(m.group(1));
                        }
                        returnVal.put("description", description.toString());

                    }
                } catch (Exception e) {
                    System.out.println("Broke doing regexDescription");
                }

                long end = System.currentTimeMillis();
                System.out.println("Time elapsed: " + (end - start));

                return returnVal;
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Broke fetching website");
            }

            try {
                return new JSONObject().put("Borked", true);
            } catch (JSONException e) {
                e.printStackTrace();
                return new JSONObject();
            }

        }



    }

}
