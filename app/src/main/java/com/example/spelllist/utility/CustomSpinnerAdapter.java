package com.example.spelllist.utility;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.spelllist.R;

import java.util.ArrayList;

public class CustomSpinnerAdapter extends ArrayAdapter<String> {
    public Resources res;
    private LayoutInflater inflater;
    private ArrayList<String> mData;


    public CustomSpinnerAdapter(Context context, ArrayList<String> objects){
        super(context, R.layout.spinner_values, objects);
        Context mContext = context;
        mData = objects;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int pos, View convertView, ViewGroup parent) {
        return getCustomView(pos, convertView, parent);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        return getCustomView(pos, convertView, parent);
    }

    private View getCustomView(int pos, View convertView, ViewGroup parent) {
        View row = inflater.inflate(R.layout.spinner_values, parent, false);
        TextView spinnerEntry = row.findViewById(R.id.spinnerValue);

        spinnerEntry.setText(mData.get(pos));

        return row;
    }


}
