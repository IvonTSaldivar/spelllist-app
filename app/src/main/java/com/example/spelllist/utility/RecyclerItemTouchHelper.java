package com.example.spelllist.utility;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.example.spelllist.R;
import com.example.spelllist.main.ItemListActivity;

public class RecyclerItemTouchHelper extends ItemTouchHelper.SimpleCallback {
    private RecyclerItemTouchHelperListener mListener;
    private int prepareColor;
    private int castColor;


    public RecyclerItemTouchHelper(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener listener){
        super(dragDirs, swipeDirs);
        mListener = listener;
        prepareColor = ItemListActivity.returnContext().getResources().getColor(R.color.bg_row_background_prepare);
        castColor = ItemListActivity.returnContext().getResources().getColor(R.color.bg_row_background_cast);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target){
        return false;
    }

    @Override
    public float getSwipeEscapeVelocity(float val){
        return 100;
    }

    @Override
    public float getSwipeThreshold(RecyclerView.ViewHolder moved){
        return .01f;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState){
        if(viewHolder != null){
            final View foregroundView = ((ItemListActivity.SimpleItemRecyclerViewAdapter.ViewHolder) viewHolder).viewForeground;
            getDefaultUIUtil().onSelected(foregroundView);
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder,
                                float dX, float dY,
                                int actionState, boolean isActive){
        final View foregroundView = ((ItemListActivity.SimpleItemRecyclerViewAdapter.ViewHolder) viewHolder).viewForeground;

        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
                actionState, isActive);

    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isActive){
            if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                if (dX < 0) {
                    ((ItemListActivity.SimpleItemRecyclerViewAdapter.ViewHolder) viewHolder).viewBackground.setBackgroundColor(prepareColor);
                } else {
                    ((ItemListActivity.SimpleItemRecyclerViewAdapter.ViewHolder) viewHolder).viewBackground.setBackgroundColor(castColor);
                }

                final View foregroundView = ((ItemListActivity.SimpleItemRecyclerViewAdapter.ViewHolder) viewHolder).viewForeground;
                if (isActive) {
                    if (dX/3 > viewHolder.itemView.getWidth() * .4f){
                        dX = viewHolder.itemView.getWidth() * .4f * 3;
                    }
                    else if (dX/3 < viewHolder.itemView.getWidth() * -.4f){
                        dX = viewHolder.itemView.getWidth() * -.4f * 3;
                    }
                    getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX / 2.9f, dY,
                            actionState, isActive);
                } else viewHolder.itemView.setTranslationX(0);
            }
       else super.onChildDraw(c, recyclerView, viewHolder, dX,dY, actionState, isActive);

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mListener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
        getDefaultUIUtil().clearView(((ItemListActivity.SimpleItemRecyclerViewAdapter.ViewHolder)viewHolder).viewForeground);
    }




    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }

}
