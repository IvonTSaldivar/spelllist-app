package com.example.spelllist.spell;


import android.content.Context;

import com.example.spelllist.main.ItemListActivity;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SpellList {
    public static List<Spell> ITEMS = new ArrayList<>();
    public static List<JSONObject> ITEMS_JSON = new ArrayList<>();
    public static Map<String, String> nameIDPairs = new HashMap<>();
    public static Map<String, Spell> ITEM_MAP = new HashMap<>();

    static {
        readSpellsFromFile(ItemListActivity.returnContext());
    }

    public static void deleteFromPosition(){

    }

    public static void deleteSingularSpell(Spell spell){
        ITEMS_JSON.remove(spell.spellJSON);
        ITEM_MAP.remove(spell.getID());
        nameIDPairs.remove(spell.getName());
        ITEMS.remove(spell);
        writeSpellsToFile(ItemListActivity.returnContext());
    }

    public static void makeNewSpell(String id, String name, String school, String level, String casting_time,
                                     String components, String range, String targets, String duration, String saving_throw, String spell_resistance, String description){
        JSONObject spellEntry = new JSONObject();
        try {
            spellEntry.put("id", id);
            spellEntry.put("name", name);
            spellEntry.put("school", school);
            spellEntry.put("level", level);
            spellEntry.put("casting_time", casting_time);
            spellEntry.put("components", components);
            spellEntry.put("range", range);
            spellEntry.put("targets", targets);
            spellEntry.put("duration", duration);
            spellEntry.put("saving_throw", saving_throw);
            spellEntry.put("spell_resistance", spell_resistance);
            spellEntry.put("description", description);

        }  catch (JSONException e) {
            e.printStackTrace();
        }
    }



    private static void addItem(Spell spell){
        if (!nameIDPairs.containsKey(spell.getName())) {
            ITEMS_JSON.add(spell.spellJSON);
            ITEM_MAP.put(spell.getID(), spell);
            nameIDPairs.put(spell.getName(), spell.getID());
            addToITEMS(spell);
        } else{
            System.out.println("It was already there!");
        }
    }

    private static void addToITEMS(Spell spell){
        ITEMS.add(spell);
        Collections.sort(ITEMS);
    }

    public static void deleteThemAll(){
        ITEMS = new ArrayList<Spell>();
        ITEM_MAP = new HashMap<String, Spell>();
        ITEMS_JSON = new ArrayList<JSONObject>();
        nameIDPairs =  new HashMap<>();
        writeSpellsToFile(ItemListActivity.returnContext());
    }



    public static boolean writeSpellsToFile(Context context){
        try {
            File file = new File(context.getExternalFilesDir(null).getAbsolutePath(), "json-spell-list.txt");

            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            new Gson().toJson(ITEMS_JSON, bufferedWriter);
            bufferedWriter.close();

            System.out.println("Sucessfully wrote to file");
            return true;
        } catch (IOException e) {
            System.out.println("WRITING TO FILE FAILED in writeSpellsToFile");
            e.printStackTrace();
            return false;
        }
    }


    /**
     * @return returns the spell list as a json
     * @throws IOException
     */
    public static boolean readSpellsFromFile(Context context) {
        StringBuilder jsonBuilder = new StringBuilder();
        try {
            BufferedReader reader = null;
            File file = new File(context.getExternalFilesDir(null).getAbsolutePath(), "json-spell-list.txt");
            if (!file.exists())
                file.createNewFile();
            reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = reader.readLine()) != null) {
                jsonBuilder.append(line);
            }
            System.out.println("Sucessfully Read from File");
            JSONArray all = new JSONArray(jsonBuilder.toString());
            //all.getJSONObject(1).getJSONObject("nameValuePairs").get("id");
            for (int i = 0; i < all.length(); ++i) {
                addItem(new Spell(all.getJSONObject(i).getJSONObject("nameValuePairs")));
            }
            reader.close();
            Collections.sort(ITEMS);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            System.out.println("JSON EXCEPTION");

        } catch (FileNotFoundException e) {
            System.out.println("FILENOTFOUND EXCEPTION");
            e.printStackTrace();

        } catch (IOException e) {
            System.out.println("IO EXCEPTION");
            e.printStackTrace();
        }
        return false;
    }

    public static class Spell implements Comparable<Spell>{
        /*
        public final String id;
        public final String name;
        public final String school;
        public final String level;
        public final String casting_time;
        public final String components;
        public final String range;
        public final String targets;
        public final String duration;
        public final String saving_throw;
        public final String spell_resistance;
        public final String description;
        */


        public final JSONObject spellJSON;
        private final StringBuilder content;
        private final StringBuilder levels;
        private final int leastLevel;
        private int preparedCount = 0;

        public final int getPrepared(){
            return preparedCount;
        }

        public int setPrepared(int increment){
            preparedCount += increment;
            return preparedCount;
        }

        public final String getID(){
            try {
                return spellJSON.getString("id");
            } catch (JSONException e) {
                e.printStackTrace();
                return e.toString();
            }
        }

        public final void setID(String id){
            try {
                spellJSON.put("id", id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public String getLevel() {
            return Integer.toString(leastLevel);
        }

        public final String getName() {
            try {
                return spellJSON.getString("name");
            } catch (JSONException e) {
                e.printStackTrace();
                return "ERROR";
            }
        }

        public Spell(JSONObject json) {
            this.spellJSON = json;
            levels = new StringBuilder();
            int tempLeast = -1;
            try {
//
//                Pattern shamanPattern = Pattern.compile("(shaman) (\\d)");
//                Matcher matcher = shamanPattern.matcher(spellJSON.get("level").toString());
//                if(matcher.find()){
//                    levels.append(matcher.group(2));
//                    tempLeast = Integer.parseInt(matcher.group(2));
//                } else {
//                    matcher = (Pattern.compile("(cleric) (\\d)")).matcher(spellJSON.get("level").toString());
//                    Matcher matcher2 = (Pattern.compile("(wizard) (\\d)")).matcher(spellJSON.get("level").toString());
//                    if(matcher.find()){
//                        levels.append(matcher.group(2));
//                        tempLeast = Integer.parseInt(matcher.group(2));
//                        if(matcher2.find()){
//                            levels.append("/").append(matcher2.group(2));
//                        }
//                    } else if (matcher2.find()){
//                        levels.append(matcher2.group(2));
//                        tempLeast = Integer.parseInt(matcher2.group(2));
//                    } else {
//                        matcher2 = Pattern.compile("\\d").matcher(spellJSON.get("level").toString());
//                        if(matcher2.find())
//                            tempLeast = Integer.parseInt(matcher.group(0));
//                    }
//                }
                String pattern = "(" + json.get("level_value").toString() + ") (\\d)";
                System.out.println("Pattern: " + pattern);
                Pattern classPattern = Pattern.compile(pattern);

                Matcher matcher = classPattern.matcher(spellJSON.get("level").toString());
                if(matcher.find()){
                    levels.append(matcher.group(2));
                    tempLeast = Integer.parseInt(matcher.group(2));
                }
                else {
                    Matcher matcher2 = (Pattern.compile("(sorcerer) (\\d)")).matcher(spellJSON.get("level").toString());
                    if(matcher2.find()){
                        levels.append(matcher.group(2));
                        tempLeast = Integer.parseInt(matcher.group(2));
                    } else {
                        matcher = Pattern.compile("\\d").matcher(spellJSON.get("level").toString());
                        if (matcher.find()) {
                            tempLeast = Integer.parseInt(matcher.group(0));
                        }
                    }
                }
            } catch (Exception e){
                try {
                    System.out.println("Error finding leastLevel, " + spellJSON.get("name"));
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
            leastLevel = tempLeast;

            content = new StringBuilder();
            try {
                spellJSON.put("least_level", leastLevel);

                spellJSON.put("id",nameIDPairs.size());
                if(json.has("name"))
                    content.append("<b>").append(json.getString("name")).append("</b><br>");
                if(json.has("school"))
                    content.append("<b>School</b> ").append(json.getString("school"));
                if(json.has("level"))
                    content.append(" <b>Level</b> ").append(json.getString("level")).append("<br>");
                if(json.has("casting_time"))
                    content.append("<b>Casting Time</b> ").append(json.getString("casting_time")).append("<br>");
                if(json.has("components"))
                    content.append("<b>Components</b> ").append(json.getString("components")).append("<br>");
                if(json.has("range"))
                    content.append("<b>Range</b> ").append(json.getString("range")).append("<br><b>");
                if(json.has("target_tag"))
                    content.append(json.getString("target_tag")).append("</b>").append(json.getString("target")).append("<br>");
                if(json.has("duration"))
                    content.append("<b>Duration</b> ").append(json.getString("duration")).append("<br>");
                if(json.has("saving_throw"))
                    content.append("<b>Saving Throw</b> ").append(json.getString("saving_throw"));
                if(json.has("spell_resistance"))
                    content.append(" <b>Spell Resistance</b> ").append(json.getString("spell_resistance")).append("<br>");
                if(json.has("description"))
                    content.append("<b>Description</b> <br>").append(json.getString("description")).append("<br>");
                addItem(this);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public String toString() {
            return content.toString();
        }

        @Override
        public int compareTo(Spell that) {
            if(this.leastLevel == that.leastLevel)
                return (this.getName().compareToIgnoreCase(that.getName()));
            else if(this.leastLevel < that.leastLevel)
                    return 1;
            else return -1;
        }
    }
}

